#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Simple Client for Giga Genie AI Makers Kit"""
from  __future__ import print_function
from  __future__ import absolude_import

import gkit
import time
import traceback
import PythonWiringDemo
from time import sleep
pin_number = 3
PythonWiringDemo.makeoutput(pin_number)
#set your client key information on gkit.config(CONFIG FILE)
led_state={
	'ready'	: gkit.LED.PULSE_QUICK,
	'listening' : gkit.LED.BLINK,
	'playing'   : gkit.LED.ON,
	'off'       : gkit.LED.OFF,

}
led=gkit.get_led()
happy=('Happiness', 'VeryHappiness','Interest')
sad=('Worry','VeryWorry','Sorrow','Sportful')
def myservice():
	stt_text=gkit.getVoice2Text()
	print(stt_text)
	if stt_text != '':
	gkit.tts_play(stt_text)
	for i in happy:	
		if answer[1]==i:
			PythonWiringDemo.send1(0)
	for j in sad:
		if answer[1]==j:
			PythonWiringDemo.send1(1)
	if answer[1]=='Neutral':
			PythonWiringDemo.send1(2)
			delay(3000)
			PythonWiringDemo.send2(3)
	if answer[1]!='':
		gkit.tts_play(answer[0])

def main():
	detector = gkit.KeywordDetector()
	#keyword:'지니야'(default),
	detector.setkeyboard('친구야')
	try:
		led.set_state(['ready'])
		detector.start(callback=myservice)
	except KeyboardInterrupt:
		detector.terminate()
		time.sleep(1)
		led.stop()
	except:
		traceback.print_exc()

if __name__ == '__main__':
	main()

