#include <Python.h>
#include <stdio.h>
#include <wiringPi.h>
#define DATA_LOC        0             // Data located in which list? 0: OFF, 1: ON

#define LOW_VAL         550           // Value to interpret as 0
#define HIGH_VAL        1650          // Value to interpret as 1

#define RANGE1_START    17            // Where does the data start (index starting from 0)
#define RANGE1_END      24            // Till where?
#define RANGE2_START    25            // comment this #define if you do not have repeated data
#define RANGE2_END      32            // comment this #define if you do not have repeated data
#define RANGE2_INVERTED 1             // is the range2 inverted/complemented range1? 1: yes, 0: no, -1: ignore range2
unsigned int IRsignal[] = 
{
     // ON, OFF (in 10's of microseconds)
  908, 446,
  59, 54,
  59, 54,
  59, 54,
  60, 52,
  59, 54,
  59, 54,
  59, 54,
  59, 165,
  59, 165,
  59, 165,
  59, 165,
  59, 165,
  59, 165,
  59, 165,
  59, 165,
  59, 54,
  59, 165,
  59, 54,
  59, 165,
  59, 54,
  59, 54,
  59, 54,
  59, 54,
  59, 54,
  59, 54,
  59, 165,
  59, 54,
  59, 165,
  59, 165,
  59, 165,
  59, 165,
  59, 165,
  59, 3998,
  906, 223,
  59, 0};
// This procedure sends a 38KHz pulse to the IRledPin 
// for a certain # of microseconds. We'll use this whenever we need to send codes
void pulseIR(long microsecs)
{
  // we'll count down from the number of microseconds we are told to wait
 
  //cli();  // this turns off any background interrupts
 
  while (microsecs > 0)
  {
    // 38 kHz is about 13 microseconds high and 13 microseconds low
   digitalWrite(IRledPin, HIGH);  // this takes about 3 microseconds to happen
   delayMicroseconds(10);         // hang out for 10 microseconds, you can also change this to 9 if its not working
   digitalWrite(IRledPin, LOW);   // this also takes about 3 microseconds
   delayMicroseconds(10);         // hang out for 10 microseconds, you can also change this to 9 if its not working
 
   // so 26 microseconds altogether
   microsecs -= 26;
  }
 
  //sei();  // this turns them back on
}

void EncodeData(int encodeVal)
{
  int tmpEncodeVal = encodeVal;
  for(int i = RANGE1_START; i <= RANGE1_END; i++)
  {
    // Current bit is 1
    if(tmpEncodeVal & 0x01)
      IRsignal[i*2 + 1 - DATA_LOC] = HIGH_VAL/10;
    else
      IRsignal[i*2 + 1 - DATA_LOC] = LOW_VAL/10;
    
    tmpEncodeVal = tmpEncodeVal >> 1;
  }
  
  #ifdef RANGE2_START
  tmpEncodeVal = encodeVal;
  for(int i = RANGE2_START; i <= RANGE2_END; i++)
  {
    // Current bit is 1
    if(RANGE2_INVERTED)
    {
      if(tmpEncodeVal & 0x01)
        IRsignal[i*2 + 1 - DATA_LOC] = LOW_VAL/10;
      else
        IRsignal[i*2 + 1 - DATA_LOC] = HIGH_VAL/10;
    }
    else
    {
      if(tmpEncodeVal & 0x01)
        IRsignal[i*2 + 1 - DATA_LOC] = HIGH_VAL/10;
      else
        IRsignal[i*2 + 1 - DATA_LOC] = LOW_VAL/10;
    }
    tmpEncodeVal = tmpEncodeVal >> 1;
  }
  #endif
}

void SendIRCode()
{
  int arraySize = sizeof(IRsignal)/2;
  for (int i = 0; i < arraySize; i++)
  {
    pulseIR(IRsignal[i++] * 10);
    delayMicroseconds(IRsignal[i] * 10);
  }
}
//static PyObject *HelloMethod(PyObject *self, PyObject *args)
//{
//	const char *pName;
//	int x;
//
//	if (!PyArg_ParseTuple(args, "s", &pName))
//		return NULL;
//	
//	printf("Hello, %s\n", pName);
//	return Py_BuildValue("i", 123);
//}
template <int _Value> static PyObject *SetPin(PyObject *self, PyObject *args)
{
	int pinNumber;
	if (!PyArg_ParseTuple(args, "d", &pinNumber))
		return Py_BuildValue("0", 1);
	digitalWrite(pinNumber, _Value);
	return Py_BuildValue("i", 1);
}
template <int _Value> static PyObject *SetMode(PyObject *self, PyObject *args)
{
	int pinNumber;
	if (!PyArg_ParseTuple(args, "d", &pinNumber))
		return Py_BuildValue("0", 1);
	pinMode(pinNumber, _Value);
	return Py_BuildValue("i", 1);
	
}
template <int _Value> static PyObject *Send(PyObject *self, PyObject *args)
{
	int Number;
	if (!PyArg_ParseTuple(args, "d", &Number))
		return Py_BuildValue("0", 1);
	while(Number>0)
	{
		SendIRCode();
		Number-=1;
	}
	return Py_BuildValue("i", 1);
	
}
static PyMethodDef PythonWiringDemoMethods[] = 
{
	//	{
	//		"hello",
	//		HelloMethod,
	//		METH_VARARGS,
	//		"Displays a \"Hello\" message." 
	//	},
	{
		"setHigh",
		SetPin<HIGH>,
		METH_VARARGS,
		"Sets an output pin value to HIGH"
	},
	{
		"setlow",
		SetPin<LOW>,
		METH_VARARGS,
		"Sets an output pin value to LOW"
	},
	{
		"makeoutput",
		SetMode<OUTPUT>,
		METH_VARARGS,
		"Configures a pin as output"
	},
	{
		"send",
		Send<OUTPUT>,
		METH_VARARGS,
		"Send N codes"
	},
	{ NULL, NULL, 0, NULL }        /* End of list */
};

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef PythonWiringDemoModule = {
	PyModuleDef_HEAD_INIT,
	"PythonWiringDemo",
	NULL,
	0,
	PythonWiringDemoMethods,
	NULL,
	NULL,
	NULL,
	NULL
};

PyMODINIT_FUNC PyInit_PythonWiringDemo(void)
{
	wiringPiSetupGpio();
	PyObject *m = PyModule_Create(&PythonWiringDemoModule);
	return m;
}

#else

PyMODINIT_FUNC initPythonWiringDemo(void)
{
	PyObject *m = Py_InitModule("PythonWiringDemo", PythonWiringDemoMethods);
	if (!m)
		return;
}

#endif